# PF1E Universal Compendium

This projects aim to be a universal compendium for Pathfinder 1E.

Its goal is to provide a unique place where anyone can find feats, races, spells and more while allowing everyone to keep the data up to date and translated.

To do that this project will provide the following functionalities :


- Filtering by source (Choosing which books the content comes from)
- Being able to translate everything
- Provide a web interface to access the content
- Provide REST APIs to allow other applications to use the compendium as a reference.

Editing the entries in the compendium should be easy and work with a review system.