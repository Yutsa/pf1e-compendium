export default interface Feat {
  category: string;
  name: string;
  flavorText: string;
  condition: string;
  advantage: string;
  normal: string;
  special: string;
}
