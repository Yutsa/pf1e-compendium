package compendium.pf1e.model.converters

import compendium.pf1e.model.PageReference
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TestPageReferenceConverter {
  @Test
  internal fun `should convert a page reference to a string to be stored using its toString method`() {
    val pageReference = PageReference(234, 256)
    val databaseValue = PageReferenceConverter().convertToDatabaseColumn(pageReference)
    assertThat(databaseValue).isEqualTo(pageReference.toString())
  }

  @Test
  internal fun `should convert a page reference database column with one page to an instance of PageReference`() {
    val pageReference = PageReference(253)
    val entityValue = PageReferenceConverter().convertToEntityAttribute(pageReference.toString())
    assertThat(entityValue).isEqualTo(pageReference)
  }

  @Test
  internal fun `should convert a page reference database column with several pages to an instance of PageReference`() {
    val pageReference = PageReference(253, 269)
    val entityValue = PageReferenceConverter().convertToEntityAttribute(pageReference.toString())
    assertThat(entityValue).isEqualTo(pageReference)
  }
}