package compendium.pf1e.model

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test

internal class TestPageReference {
  @Test
  internal fun `should print only one page if there's only on page referenced`() {
    val pageReference = PageReference(234)
    assertThat(pageReference.toString()).isEqualTo("p. 234")
  }

  @Test
  internal fun `should print first and last pages separated by a dash if there's several pages referenced`() {
    val pageReference = PageReference(234, 236)
    assertThat(pageReference.toString()).isEqualTo("p. 234-236")
  }
}