package compendium.pf1e.bootstrap

import compendium.pf1e.model.*
import compendium.pf1e.repositories.FeatRepository
import compendium.pf1e.repositories.SourceMaterialRepository
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class DatabaseInitializer(
  var sourceMaterialRepository: SourceMaterialRepository,
  var featRepository: FeatRepository
) : CommandLineRunner {
  val LOGGER = LoggerFactory.getLogger(DatabaseInitializer::class.java)

  override fun run(vararg args: String?) {
    initializeSourceMaterials()
  }

  fun initializeSourceMaterials() {
    LOGGER.info("Initializing source materials")
    val manuelDesJoueurs = SourceMaterial("MdJ", "Manuel des Joueurs", MaterialType.BOOK,
      "https://www.black-book-editions.fr/produit.php?id=4445")
    sourceMaterialRepository.save(manuelDesJoueurs)
    val pageReference = PageReference(234, 256)
    val reference = SourceReference(manuelDesJoueurs, pageReference)
    val feat = Feat(manuelDesJoueurs.id + "0001", "Adepte de la matraque", reference)
    featRepository.save(feat)
  }
}