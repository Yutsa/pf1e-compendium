package compendium.pf1e.model

class PageReference(
  var firstPage: Int,
  var lastPage: Int? = null
) {

  override fun toString(): String {
    val firstPart = "p. $firstPage"
    val lastPart = if (lastPage != null) "-$lastPage" else ""
    return firstPart + lastPart
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as PageReference

    if (firstPage != other.firstPage) return false
    if (lastPage != other.lastPage) return false

    return true
  }

  override fun hashCode(): Int {
    var result = firstPage
    result = 31 * result + (lastPage ?: 0)
    return result
  }
}