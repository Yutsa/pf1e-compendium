package compendium.pf1e.model

enum class MaterialType {
  BOOK, BLOG
}