package compendium.pf1e.model

import compendium.pf1e.model.converters.PageReferenceConverter
import javax.persistence.*

@Embeddable
class SourceReference(
  @ManyToOne var sourceMaterial: SourceMaterial,
  @Convert(converter = PageReferenceConverter::class) var pageReference: PageReference
)