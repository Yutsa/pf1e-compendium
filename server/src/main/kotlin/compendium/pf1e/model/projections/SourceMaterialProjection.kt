package compendium.pf1e.model.projections

import compendium.pf1e.model.SourceMaterial
import org.springframework.data.rest.core.config.Projection

@Projection(name = "source-material", types = [SourceMaterial::class])
interface SourceMaterialProjection {
  fun getName() : String
}