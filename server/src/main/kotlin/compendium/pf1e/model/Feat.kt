package compendium.pf1e.model

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Feat(
  @Id var id : String,
  var name : String,
  var reference : SourceReference
)