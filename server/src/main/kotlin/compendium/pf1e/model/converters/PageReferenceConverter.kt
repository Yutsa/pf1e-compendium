package compendium.pf1e.model.converters

import compendium.pf1e.model.PageReference
import javax.persistence.AttributeConverter

class PageReferenceConverter : AttributeConverter<PageReference, String> {
  override fun convertToDatabaseColumn(pageReference: PageReference?): String {
    return pageReference.toString()
  }

  override fun convertToEntityAttribute(pageReferenceInDatabase: String?): PageReference {
    if (pageReferenceInDatabase == null) throw IllegalArgumentException()
    val pages = pageReferenceInDatabase.removePrefix("p. ").split("-")
    val firstPage = pages[0].toInt()
    val lastPage = if (pages.size > 1) pages[1].toInt() else null
    return PageReference(firstPage, lastPage)
  }
}