package compendium.pf1e.model

import javax.persistence.*

@Entity
class SourceMaterial(
  @Id var id: String,
  var name: String,
  @Enumerated(EnumType.STRING) var type: MaterialType,
  var reference: String
)