package compendium.pf1e.repositories

import compendium.pf1e.model.SourceMaterial
import compendium.pf1e.model.projections.SourceMaterialProjection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(excerptProjection = SourceMaterialProjection::class)
interface SourceMaterialRepository : JpaRepository<SourceMaterial, String>