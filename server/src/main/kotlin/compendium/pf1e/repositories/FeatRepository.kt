package compendium.pf1e.repositories

import compendium.pf1e.model.Feat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface FeatRepository : JpaRepository<Feat, String>